import React from 'react'
import ReactDOM from 'react-dom';
class Counter extends React.Component{
	    constructor(props) {
        super();
			this.state = {value: 0}
			this.countDown = this.countDown.bind(this)
		}
		countDown(){
			ReactDOM.unmountComponentAtNode(
				ReactDOM.findDOMNode(this)
					.parentNode.parentNode
			);
		}
			
		clicks () {
			this.state.value += 1;
				this.state.value > 2   ? this.countDown()   
			:	this.setState({	value: this.state.value })
			
		}
	componentWillUnmount(){
		alert('componentWillUnmount')
	}

		render(){
			return (
				<div id="container">
					<div>{this.state.value}</div>
			<button onClick = {this.clicks.bind(this)} value={this.state.value}>+</button>
				</div>
			)
    }
}

export default Counter