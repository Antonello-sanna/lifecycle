import React from 'react';
import Banana from './Banana'
import MrBanana from './CustomerPage'
import Counter from './Counter'

class App extends React.Component {
	constructor(props){
		super()
		this.state = {name:''}
		this.getData = this.getData.bind(this)
	}
	componentWillMount(){
		this.setState({name:'Jason'})
	}
	
	getData(e){
		e.preventDefault()
		debugger
		var data = this.refs.here.value
		return data
	}
  render() {
    return (
      <div className="App">
     <Banana/>
     <MrBanana name= 'jason bourne' age = '23'/>
     <Counter/>
      </div>
    );
  }
}

export default App;
