import React from 'react'

class Banana extends React.Component{
	constructor(props){
		super()
		this.state={banana:''}
		this.Data = this.Data.bind(this)
	}
	
	Data(){
		this.setState({
			banana:'banana'
		})

		
	}
	
	componentDidUpdate(prop,state){
		console.log("this component Did Update!")
		
	}
	render(){
		return(<div>
			<button onClick={this.Data.bind(this)}>update on click</button>
		</div>)
	}
}

export default Banana